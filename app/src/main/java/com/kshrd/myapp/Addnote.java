package com.kshrd.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Addnote extends AppCompatActivity {

    private EditText txtTitle, txtdesc;
    private Button btnsave, btncancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnote);

        txtTitle = findViewById(R.id.txttitle);
        txtdesc = findViewById(R.id.txtdesc);
        btnsave = findViewById(R.id.btnsave);
        btncancel = findViewById(R.id.btncancel);
        Intent intent = getIntent();

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtTitle.getText().toString().trim().length() > 0 && txtdesc.getText().toString().trim().length() > 0){
                    intent.putExtra("title", txtTitle.getText().toString());
                    intent.putExtra("desc", txtdesc.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else{
                    Toast.makeText(Addnote.this, "Please input information above.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED,intent);
                finish();
            }
        });
    }
}