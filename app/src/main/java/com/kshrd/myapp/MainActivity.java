package com.kshrd.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Homework002");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void handleOnClick(View view){
        Intent intent;
        switch (view.getId()){
            case R.id.gallery:
                intent = new Intent(getApplicationContext(), GellaryActivity.class);
                startActivity(intent);
                break;
            case R.id.contact:
                intent = new Intent(getApplicationContext(), ContactActivity.class);
                startActivity(intent);
                break;
            case R.id.note:
                intent = new Intent(getApplicationContext(), Notelist.class);
                startActivity(intent);
                break;
            case R.id.call:
                intent = new Intent(getApplicationContext(), CallActivity.class);
                startActivity(intent);
                break;
            case R.id.profile:
                intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}