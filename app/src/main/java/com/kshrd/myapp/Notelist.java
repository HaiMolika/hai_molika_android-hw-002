package com.kshrd.myapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Notelist extends AppCompatActivity {

    private GridLayout rootview;
    private FrameLayout frameLayout;
    private TextView header, description;
    private FrameLayout addnote;
    private String title = "hello", text="Lala";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notelist);
        rootview = findViewById(R.id.gridlayout);
        addnote = findViewById(R.id.addnote);

        addnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notelist.this, Addnote.class);
                startActivityForResult(intent,1);
            }
        });

        if(getIntent().getExtras() != null){
//            Toast.makeText(this, "not object", Toast.LENGTH_SHORT).show();

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK)
                //initialize view
                frameLayout = new FrameLayout(this);
                header = new TextView(this);
                description = new TextView(this);

                //configure header
                FrameLayout.LayoutParams header_style = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                header_style.setMargins(0, 30, 0, 0);
                header.setLayoutParams(header_style);
                header.setTextColor(getResources().getColor(R.color.white, null));
                header.setGravity(Gravity.CENTER_HORIZONTAL);
                header.setTypeface(Typeface.DEFAULT_BOLD);
                header.setTextSize(25);
                header.setText(getIntent().getStringExtra("title"));

                //configure description
                FrameLayout.LayoutParams desc_style = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                header_style.setMargins(0, 65, 0, 0);
                description.setLayoutParams(desc_style);
                description.setTextColor(getResources().getColor(R.color.white, null));
                description.setGravity(Gravity.CENTER_HORIZONTAL);
                description.setText(getIntent().getStringExtra("desc"));

                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(200, 150);
                layoutParams.width = rootview.getMeasuredWidthAndState();
                frameLayout.setLayoutParams(layoutParams);
                frameLayout.setPadding(10, 10, 10, 10);
                frameLayout.setBackgroundColor(getResources().getColor(R.color.frame, null));

                //add view into framelayout
                frameLayout.addView(header);
                frameLayout.addView(description);
                //add framelayout into gridlayout

                GridLayout.LayoutParams params = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f));

                int row = rootview.getRowCount();
                int column = rootview.getColumnCount();
                Toast.makeText(this, column + "", Toast.LENGTH_SHORT).show();
                if(column%2 == 0){
                    params.rowSpec = GridLayout.spec(row);
                    params.columnSpec = GridLayout.spec(1);
                }
                else{
                    params.rowSpec = GridLayout.spec(row+1);
                    params.columnSpec = GridLayout.spec(0);
            }
            frameLayout.setLayoutParams(params);
            rootview.addView(frameLayout);
        }
    }
}